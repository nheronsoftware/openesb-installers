/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU General
 * Public License Version 2 only ("GPL") or the Common Development and Distribution
 * License("CDDL") (collectively, the "License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html or nbbuild/licenses/CDDL-GPL-2-CP. See the
 * License for the specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header Notice in
 * each file and include the License file at nbbuild/licenses/CDDL-GPL-2-CP.  Sun
 * designates this particular file as subject to the "Classpath" exception as
 * provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the License Header,
 * with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * The Original Software is NetBeans. The Initial Developer of the Original Software
 * is Sun Microsystems, Inc. Portions Copyright 1997-2007 Sun Microsystems, Inc. All
 * Rights Reserved.
 * 
 * If you wish your version of this file to be governed by only the CDDL or only the
 * GPL Version 2, indicate your decision by adding "[Contributor] elects to include
 * this software in this distribution under the [CDDL or GPL Version 2] license." If
 * you do not indicate a single choice of license, a recipient has the option to
 * distribute your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above. However, if
 * you add GPL Version 2 code and therefore, elected the GPL Version 2 license, then
 * the option applies only if the new code is made subject to such option by the
 * copyright holder.
 */

package org.netbeans.installer.wizard.components.actions;

import java.io.ByteArrayInputStream;
import java.util.List;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import javax.swing.text.AttributeSet;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;
import org.netbeans.installer.Installer;
import org.netbeans.installer.product.components.Product;
import org.netbeans.installer.product.Registry;
import org.netbeans.installer.utils.helper.Status;
import org.netbeans.installer.utils.helper.ErrorLevel;
import org.netbeans.installer.utils.ErrorManager;
import org.netbeans.installer.utils.LogManager;
import org.netbeans.installer.utils.ResourceUtils;
import org.netbeans.installer.utils.StringUtils;
import org.netbeans.installer.utils.SystemUtils;
import org.netbeans.installer.utils.FileUtils;
import org.netbeans.installer.utils.exceptions.UninstallationException;
import org.netbeans.installer.utils.exceptions.XMLException;
import org.netbeans.installer.utils.progress.CompositeProgress;
import org.netbeans.installer.utils.progress.Progress;
import org.netbeans.installer.wizard.components.WizardAction;
import org.netbeans.installer.utils.helper.DetailedStatus;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class UninstallAction extends WizardAction {
    /////////////////////////////////////////////////////////////////////////////////
    // Constants
    public static final String DEFAULT_TITLE =
            ResourceUtils.getString(UninstallAction.class,
            "UA.title"); // NOI18N
    public static final String DEFAULT_DESCRIPTION =
            ResourceUtils.getString(UninstallAction.class,
            "UA.description"); // NOI18N
    public static final String DEFAULT_PROGRESS_UNINSTALL_TITLE_MESSAGE =
            ResourceUtils.getString(UninstallAction.class,
            "UA.progress.uninstall.title");//NOI18N
    public static final String DEFAULT_UNINSTALL_DEPENDENT_FAILED_MESSAGE =
            ResourceUtils.getString(UninstallAction.class,
            "UA.uninstall.dependent.failed");//NOI18N

    public static final String PROGRESS_UNINSTALL_TITLE_PROPERTY =
            "progress.uninstall.title";

    public static final String UNINSTALL_DEPENDENT_FAILED_PROPERTY =
            "uninstall.dependent.failed";
    public static final int UNINSTALLATION_ERROR_CODE =
            126;//NOMAGI

    /////////////////////////////////////////////////////////////////////////////////
    // Instance
    private CompositeProgress overallProgress;
    private Progress          currentProgress;
    private String mServiceTagXMLVal = null;

    public UninstallAction() {
        setProperty(TITLE_PROPERTY, DEFAULT_TITLE);
        setProperty(DESCRIPTION_PROPERTY, DEFAULT_DESCRIPTION);
        setProperty(PROGRESS_UNINSTALL_TITLE_PROPERTY,
                DEFAULT_PROGRESS_UNINSTALL_TITLE_MESSAGE);
        setProperty(UNINSTALL_DEPENDENT_FAILED_PROPERTY,
                DEFAULT_UNINSTALL_DEPENDENT_FAILED_MESSAGE);
    }

    public void execute() {
        LogManager.logIndent("Start products uninstallation");
        String GFHome = null;
        String NBHome = null;
        boolean isGFUninstall = false;
        boolean isNBUninstall = false;

        final Registry registry = Registry.getInstance();
        final List<Product> products = registry.getProductsToUninstall();
        final int percentageChunk = Progress.COMPLETE / products.size();
        final int percentageLeak = Progress.COMPLETE % products.size();

        overallProgress = new CompositeProgress();
        overallProgress.setPercentage(percentageLeak);
        overallProgress.synchronizeDetails(true);

        getWizardUi().setProgress(overallProgress);
        for (int i = 0; i < products.size(); i++) {
            // get the handle of the current item
            final Product product = products.get(i);
            if (product.getUid().equals("nb-base")) {
                isNBUninstall = true;
                NBHome = product.getInstallationLocation().getAbsolutePath();
                File toDeleteFile = new File(NBHome + File.separator + "register-glassfishesb.html");
                if (toDeleteFile.exists()) {
                    parseHTML(toDeleteFile.getAbsolutePath());
                    toDeleteFile.delete();
                }
                toDeleteFile = new File(NBHome + File.separator + "nb" + File.separator + "servicetag" + File.separator + "product.properties");
                if (toDeleteFile.exists()) {
                    toDeleteFile.delete();
                }
                toDeleteFile = new File(NBHome + File.separator + "nb" + File.separator + "servicetag" + File.separator + "registration.xml");
                if (toDeleteFile.exists()) {
                    toDeleteFile.delete();
                }
            }
            if (product.getUid().equals("glassfish")) {
                isGFUninstall = true;
                GFHome = product.getInstallationLocation().getAbsolutePath();
                File toDeleteFile = new File(GFHome + File.separator + "register-glassfishesb.html");
                if (toDeleteFile.exists()) {
                    parseHTML(toDeleteFile.getAbsolutePath());
                    toDeleteFile.delete();
                }

            }

            // initiate the progress for the current element
            currentProgress = new Progress();

            overallProgress.addChild(currentProgress, percentageChunk);
            overallProgress.setTitle(StringUtils.format(
                    getProperty(PROGRESS_UNINSTALL_TITLE_PROPERTY),
                    product.getDisplayName()));
            try {
                product.uninstall(currentProgress);

                // sleep a little so that the user can perceive that something
                // is happening
                SystemUtils.sleep(200);
            }  catch (UninstallationException e) {
                // do not override already set exit code
                if (System.getProperties().get(Installer.EXIT_CODE_PROPERTY) == null) {
                     System.getProperties().put(Installer.EXIT_CODE_PROPERTY,
                             new Integer(UNINSTALLATION_ERROR_CODE));
                }
                // adjust the component's status and save this error - it will
                // be reused later at the PostInstallSummary
                product.setStatus(Status.INSTALLED);
                product.setUninstallationError(e);

                // since the product failed to uninstall  - we should remove
                // the components it depends on from our plans to uninstall
                for(Product requirement : registry.getProducts()) {
                    if ((requirement.getStatus() == Status.TO_BE_UNINSTALLED) &&
                            registry.satisfiesRequirement(requirement, product)) {
                        UninstallationException requirementError =
                                new UninstallationException(
                                StringUtils.format(
                                getProperty(PROGRESS_UNINSTALL_TITLE_PROPERTY),
                                requirement.getDisplayName(),
                                product.getDisplayName()), e);

                        requirement.setStatus(Status.INSTALLED);
                        requirement.setUninstallationError(requirementError);

                        products.remove(requirement);
                    }
                }

                // finally notify the user of what has happened
                ErrorManager.notify(ErrorLevel.ERROR, e);
            }
        }

        // reset properties
        System.getProperties().put("GF_REGISTRY_GLASSFISHESB_HTML", "");
        System.getProperties().put("GF_UNINSTALL", "false");
        System.getProperties().put("NB_REGISTRY_GLASSFISHESB_HTML", "");
        System.getProperties().put("NB_UNINSTALL", "false");
        System.getProperties().put("SERVICE_TAG_INFO", "");
        
        // set properties
        if (isNBUninstall) {
            System.getProperties().put("NB_REGISTRY_GLASSFISHESB_HTML",
                    NBHome + File.separator + "register-glassfishesb.html");
            System.getProperties().put("NB_UNINSTALL", "true");
	    if(mServiceTagXMLVal!=null) {
                System.getProperties().put("SERVICE_TAG_INFO", mServiceTagXMLVal);
	    }
        }
        if (isGFUninstall) {
            System.getProperties().put("GF_REGISTRY_GLASSFISHESB_HTML",
                    GFHome + File.separator + "register-glassfishesb.html");
            System.getProperties().put("GF_UNINSTALL", "true");
	    if(mServiceTagXMLVal!=null) {
                System.getProperties().put("SERVICE_TAG_INFO", mServiceTagXMLVal);
	    }
        }
        LogManager.logUnindent("... finished products uninstallation");
    }
    
    @Override
    public boolean canExecuteForward() {
        return Registry.getInstance().getProductsToUninstall().size() > 0;
    }

    @Override
    public boolean isPointOfNoReturn() {
        return true;
    }

    @Override
    public boolean isCancelable() {
        return false;
    }

    /**
     * Parse the register-glassfishesb.html and get registration data section
     * @param regFile
     */
    public void parseHTML(String regFile) {
        ParserGetter kit = new ParserGetter();
        HTMLEditorKit.Parser parser = kit.getParser();
        HTMLEditorKit.ParserCallback callback = new ReportAttributes();
        final File theRegistrationXMLFile = new File(regFile);
        try {
            InputStream in = new FileInputStream(theRegistrationXMLFile);
            InputStreamReader r = new InputStreamReader(in);
            parser.parse(r, callback, true);
        } catch (IOException e) {
            LogManager.log(e.getMessage());
        }
    }

    class ParserGetter extends HTMLEditorKit {
        public HTMLEditorKit.Parser getParser() {
            return super.getParser();
        }
    }

    class ReportAttributes extends HTMLEditorKit.ParserCallback {
        private void listAttributes(AttributeSet attributes) {
            if (attributes.containsAttribute(HTML.Attribute.NAME, 
                    "servicetag_payload")) {  //NOI18N
                Object attrVal = attributes.getAttribute(HTML.Attribute.VALUE);
                mServiceTagXMLVal = attrVal.toString();
            }
            return;
        }

        public void handleSimpleTag(HTML.Tag tag, MutableAttributeSet attributes,
                int position) {
            if ((tag != null) && ("input".equals(tag.toString()))) { //NOI18N
                this.listAttributes(attributes);
            }
        }
    }
}
