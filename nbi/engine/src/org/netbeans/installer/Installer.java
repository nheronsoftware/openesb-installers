/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU General
 * Public License Version 2 only ("GPL") or the Common Development and Distribution
 * License("CDDL") (collectively, the "License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html or nbbuild/licenses/CDDL-GPL-2-CP. See the
 * License for the specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header Notice in
 * each file and include the License file at nbbuild/licenses/CDDL-GPL-2-CP.  Sun
 * designates this particular file as subject to the "Classpath" exception as
 * provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the License Header,
 * with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original Software
 * is Sun Microsystems, Inc. Portions Copyright 1997-2007 Sun Microsystems, Inc. All
 * Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL or only the
 * GPL Version 2, indicate your decision by adding "[Contributor] elects to include
 * this software in this distribution under the [CDDL or GPL Version 2] license." If
 * you do not indicate a single choice of license, a recipient has the option to
 * distribute your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above. However, if
 * you add GPL Version 2 code and therefore, elected the GPL Version 2 license, then
 * the option applies only if the new code is made subject to such option by the
 * copyright holder.
 */

package org.netbeans.installer;

import javax.swing.JOptionPane;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Enumeration;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.TreeSet;
import java.util.List;
import java.util.Iterator;
import java.util.Arrays;
import org.netbeans.installer.downloader.DownloadManager;
import org.netbeans.installer.product.Registry;
import org.netbeans.installer.utils.DateUtils;
import org.netbeans.installer.utils.EngineUtils;
import org.netbeans.installer.utils.SystemUtils;
import org.netbeans.installer.utils.ErrorManager;
import org.netbeans.installer.utils.LogManager;
import org.netbeans.installer.utils.ResourceUtils;
import org.netbeans.installer.utils.UiUtils;
import org.netbeans.installer.utils.cli.options.*;
import org.netbeans.installer.utils.cli.CLIHandler;
import org.netbeans.installer.utils.helper.EngineResources;
import org.netbeans.installer.utils.helper.FinishHandler;
import org.netbeans.installer.utils.progress.Progress;
import org.netbeans.installer.wizard.Wizard;

/**
 * The main class of the NBI engine. It represents the installer and provides
 * methods to start the installation/maintenance process as well as to
 * finish/cancel/break the installation.
 *
 * @author Kirill Sorokin
 */
public class Installer implements FinishHandler {
    /////////////////////////////////////////////////////////////////////////////////
    // Main
    /**
     * The main method. It creates an instance of {@link Installer} and calls
     * the {@link #start()} method, passing in the command line arguments.
     *
     * @param arguments The command line arguments
     */
    public static void main(String[] arguments) {
        new Installer(arguments).start();
    }

    /////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Installer instance;

    /**
     * Returns an instance of <code>Installer</code>. If the instance does not
     * exist - it is created.
     *
     * @return An instance of <code>Installer</code>
     */
    public static synchronized Installer getInstance() {
        if (instance == null) {
            instance = new Installer(new String[0]);
        }

        return instance;
    }

    /////////////////////////////////////////////////////////////////////////////////
    // Instance
    private File localDirectory = null;

    // Constructor //////////////////////////////////////////////////////////////////
    /**
     * The only private constructor - we need to hide the default one as
     * <code>Installer is a singleton.
     *
     * @param arguments Command-line parameters.
     */
    private Installer(String[] arguments) {
        String invalidArgs = validateInstallArguments(arguments);

        if (invalidArgs != null) {
            if (invalidArgs.startsWith("state.file=")) {
                String errMsg = validateStateFile(new File(invalidArgs.substring(11)));
                if (errMsg != null) {
                    String title = "State File Validation";
                    String displayMsg = "There are error(s) found in your state file: \n" + errMsg;
                    if (isDisplayEnvSet()) {
                        JOptionPane.showMessageDialog(null, displayMsg, title, JOptionPane.ERROR_MESSAGE);
                    } else {
                        LogManager.logEntry(displayMsg);
                        System.out.println(displayMsg);
                    }
                    criticalExit();
                }
            } else if (invalidArgs.startsWith("record.file=")) {
                // future validation for record file
            } else {
                String title = "Command Line Validation";
                String displayMsg = "Invalid command line option(s) were found: " + invalidArgs + "\n\n" +
                                    "Option Usage can be the following:\n" +
                                    "   1. --record <state-file-path>\n" +
                                    "   2. --silent\n" +
                                    "   3. --silent --state <state-file-path>\n" +
                                    "   4. --silent --record <state-file-path>\n";
                if (isDisplayEnvSet()) {
                    JOptionPane.showMessageDialog(null, displayMsg, title, JOptionPane.ERROR_MESSAGE);
                } else {
                    LogManager.logEntry(displayMsg);
                    System.out.println(displayMsg);
                }
                criticalExit();
            }
        }

        LogManager.logEntry("initializing the installer engine"); // NOI18N

        initializeErrorHandler();
        initializePlatform();


        instance = this;

        dumpSystemInfo();
        EngineUtils.checkEngine();
        parseArguments(arguments);
        loadEngineProperties();
        initializeLocalDirectory();

        // once we have set the local directory (and therefore devised the log
        // file path) we can start logging safely
        initializeLogManager();
        initializeDownloadManager();
        initializeRegistry();
        initializeWizard();
        createLockFile();

        LogManager.logExit("... finished initializing the engine"); // NOI18N
    }

    public String validateInstallArguments(String [] arguments) {
        if (arguments.length == 0) {
            return null;
        }
        // It's some valid options while building the installer or uninstall
        for (int i = 0; i < arguments.length; i++) {
            if (arguments[i].equals("--create-bundle") ||
                arguments[i].equals("--target") ||
                arguments[i].equals("--force-uninstall")) {
                return null;
            }
        }
        int argCount = arguments.length;
        // check the error while in installation
        if (argCount == 1) {
            if (arguments[0].equalsIgnoreCase("--SILENT")) {
                // this is intentional as a lone '--silent' is supported
                return null;
            }
            if (arguments[0].equalsIgnoreCase("--RECORD")) {
                return "The required state file is missing if you use --record option.";
            } else {
                return "The command line option is not valid.";
            }
        } else if (argCount == 2) {
            if (arguments[0].equalsIgnoreCase("--SILENT") &&
                arguments[1].equalsIgnoreCase("--STATE")) {
                return "The required state file is missing if you use --state option.";
            } else if (arguments[0].equalsIgnoreCase("--RECORD") &&
                       !arguments[1].startsWith("--")) {
                    return null;
            } else {
                return "The command line options are not valid.";
            }
        } else if (argCount == 3) {
            if (arguments[0].equalsIgnoreCase("--SILENT") &&
                arguments[1].equalsIgnoreCase("--STATE") &&
                !arguments[2].startsWith("--")) {
                    return "state.file=" + arguments[2];
            } else if (arguments[0].equalsIgnoreCase("--SILENT") &&
                arguments[1].equalsIgnoreCase("--RECORD") &&
                !arguments[2].startsWith("--")) {
                    return "record.file=" + arguments[2];
            } else {
                return "The command line options are not valid.";
            }
        } else if (argCount > 3) {
            return "Too many command line options. Some of them are not valid options.";
        }
        return null;
    }

    public String validateStateFile(File stateFile) {
        if (!stateFile.exists()) {
            return "The state file " + stateFile.getAbsolutePath() + " does not exist.";
        }
        String errMsg = "";
        boolean hasError = false;
        try {
            String platform = null;
            if (SystemUtils.isWindows()) {
                platform = "windows";
            } else if (SystemUtils.isLinux()) {
                platform = "linux";
            } else if (SystemUtils.isSolaris()) {
                platform = "solaris";
            } else if (SystemUtils.isMacOS()) {
                platform = "macosx";
            } else if (SystemUtils.isAIX()) {
                platform = "aix";
            } else {
                platform = "unsupport";
            }
            BufferedReader input =  new BufferedReader(new FileReader(stateFile));
            String line = null;
            /*
            * readLine is a bit quirky :
            * it returns the content of a line MINUS the newline.
            * it returns null only for the END of the stream.
            * it returns an empty String if two newlines appear in a row.
            */
            while (( line = input.readLine()) != null){
                if (line.equals("")) {
                    continue;
                }
                if (line.indexOf("installation.location." + platform) > 0) {
                    int idx1 = line.indexOf("\">");
                    int idx2 = line.lastIndexOf("</");
                    if (idx1 + 2 == idx2) {
                        errMsg += "installation.location." + platform + " can not be empty.\n";
                        hasError = true;
                        continue;
                    }
                    String path = line.substring(idx1 + 2, idx2);
                    if (!SystemUtils.isPathValid(path)) {
                        errMsg += "installation.location." + platform + path + " is not a valid path.\n";
                        hasError = true;
                        continue;
                    }
                    File folder = new File (path);
                    if (folder.exists()) {
                        errMsg += "installation.location." + platform + path + " already exists.\n";
                        hasError = true;
                        continue;
                    }
                }
                if (line.indexOf("installation.location") > 0) {
                    int idx1 = line.indexOf("\">");
                    int idx2 = line.lastIndexOf("</");
                    if (idx1 + 2 == idx2) {
                        errMsg += "installation.location can not be empty.\n";
                        hasError = true;
                        continue;
                    }
                    String path = line.substring(idx1 + 2, idx2);
                    if (path.startsWith("$N{install}")) {
                        continue;
                    }
                    if (!SystemUtils.isPathValid(path)) {
                        errMsg += "installation.location" + path + " is not a valid path.\n";
                        hasError = true;
                        continue;
                    }
                    File folder = new File (path);
                    if (folder.exists()) {
                        errMsg += "installation.location" + path + " already exists.\n";
                        hasError = true;
                        continue;
                    }
                }
                if (line.indexOf("jdk.location") > 0) {
                    int idx1 = line.indexOf("\">");
                    int idx2 = line.lastIndexOf("</");
                    if (idx1 + 2 == idx2) {
                        errMsg += "Install location can not be empty.\n";
                        hasError = true;
                        continue;
                    }
                    String path = line.substring(idx1 + 2, idx2);
                    if (!SystemUtils.isPathValid(path)) {
                        errMsg += "jdk location " + path + " is not a valid path.\n";
                        continue;
                    }
                    File folder = new File (path);
                    if (!folder.exists()) {
                        errMsg += "jdk location " + path + " does not exist.\n";
                        hasError = true;
                        continue;
                    }
                }
                if (line.indexOf("username") > 0) {
                    int idx1 = line.indexOf("\">");
                    int idx2 = line.lastIndexOf("</");
                    if (idx1 + 2 == idx2) {
                        errMsg += "Username can not be empty.\n";
                        hasError = true;
                        continue;
                    }
                    String username = line.substring(idx1 + 2, idx2);
                    if (!username.matches("[0-9a-zA-Z]+")) {
                        errMsg += "Username can only contain alphanumeric characters (no special characters).\n";
                        hasError = true;
                        continue;
                    }
                }
                if (line.indexOf("password") > 0) {
                    int idx1 = line.indexOf("\">");
                    int idx2 = line.lastIndexOf("</");
                    if (idx1 + 2 == idx2) {
                        errMsg += "Password can not be empty.\n";
                        hasError = true;
                        continue;
                    }
                    String password = line.substring(idx1 + 2, idx2);
                    if (password.length() < 8) {
                        errMsg += "Password sould contain 8 or more characters.\n";
                        hasError = true;
                        continue;
                    }
                }
                if (line.indexOf("http.port") > 0) {
                    int idx1 = line.indexOf("\">");
                    int idx2 = line.lastIndexOf("</");
                    if (idx1 + 2 == idx2) {
                        errMsg += "http.port can not be empty.\n";
                        hasError = true;
                        continue;
                    }
                    String portStr = line.substring(idx1 + 2, idx2);
                    try {
                        int port = Integer.parseInt(portStr);
                        if (port < 1 || port > 65535) {
                             errMsg += "The value of http.port should be an integer from 1 to 65535\n";
                        }
                    } catch (NumberFormatException e) {
                        errMsg += "The value of http.port must be numeric.\n";
                        hasError = true;
                        continue;
                    }
                }
                if (line.indexOf("https.port") > 0) {
                    int idx1 = line.indexOf("\">");
                    int idx2 = line.lastIndexOf("</");
                    if (idx1 + 2 == idx2) {
                        errMsg += "https.port can not be empty.\n";
                        hasError = true;
                        continue;
                    }
                    String portStr = line.substring(idx1 + 2, idx2);
                    try {
                        int port = Integer.parseInt(portStr);
                        if (port < 1 || port > 65535) {
                            errMsg += "The value of https.port should be an integer from 1 to 65535\n";
                            hasError = true;
                            continue;
                        }
                    } catch (NumberFormatException e) {
                        errMsg += "The value of https.port must be numeric.\n";
                        hasError = true;
                        continue;
                    }
                }
                if (line.indexOf("admin.port") > 0) {
                    int idx1 = line.indexOf("\">");
                    int idx2 = line.lastIndexOf("</");
                    if (idx1 + 2 == idx2) {
                        errMsg += "admin.port can not be empty.\n";
                        hasError = true;
                        continue;
                    }
                    String portStr = line.substring(idx1 + 2, idx2);
                    try {
                        int port = Integer.parseInt(portStr);
                        if (port < 1 || port > 65535) {
                            errMsg += "The value of admin.port should be an integer from 1 to 65535\n";
                            hasError = true;
                            continue;
                        }
                    } catch (NumberFormatException e) {
                        errMsg += "The value of admin.port must be numeric.\n";
                        hasError = true;
                        continue;
                    }
                }
                if (line.indexOf("jms.port") > 0) {
                    int idx1 = line.indexOf("\">");
                    int idx2 = line.lastIndexOf("</");
                    if (idx1 + 2 == idx2) {
                        errMsg += "jms.port can not be empty.\n";
                        hasError = true;
                        continue;
                    }
                    String portStr = line.substring(idx1 + 2, idx2);
                    try {
                        int port = Integer.parseInt(portStr);
                        if (port < 1 || port > 65535) {
                            errMsg += "The value of jms.port should be an integer from 1 to 65535\n";
                            hasError = true;
                            continue;
                        }
                    } catch (NumberFormatException e) {
                        errMsg += "The value of jms.port must be numeric.\n";
                        hasError = true;
                        continue;
                    }
                }
                if (line.indexOf("jmx.admin.port") > 0) {
                    int idx1 = line.indexOf("\">");
                    int idx2 = line.lastIndexOf("</");
                    if (idx1 + 2 == idx2) {
                        errMsg += "jmx.admin.port can not be empty.\n";
                        hasError = true;
                        continue;
                    }
                    String portStr = line.substring(idx1 + 2, idx2);
                    try {
                        int port = Integer.parseInt(portStr);
                        if (port < 1 || port > 65535) {
                             errMsg += "The value of jmx.admin.port should be an integer from 1 to 65535\n";
                             hasError = true;
                        }
                    } catch (NumberFormatException e) {
                        errMsg += "The value of jmx.admin.port must be numeric.\n";
                        hasError = true;
                        continue;
                    }
                }
                if (line.indexOf("iiop.port") > 0) {
                    int idx1 = line.indexOf("\">");
                    int idx2 = line.lastIndexOf("</");
                    if (idx1 + 2 == idx2) {
                        errMsg += "iiop.port can not be empty.\n";
                        hasError = true;
                        continue;
                    }
                    String portStr = line.substring(idx1 + 2, idx2);
                    try {
                        int port = Integer.parseInt(portStr);
                        if (port < 1 || port > 65535) {
                             errMsg += "The value of iiop.port should be an integer from 1 to 65535\n";
                             hasError = true;
                             continue;
                        }
                    } catch (NumberFormatException e) {
                        errMsg += "The value of iiop.port must be numeric.\n";
                        hasError = true;
                        continue;
                    }
                }
                if (line.indexOf("iiop.ssl.port") > 0) {
                    int idx1 = line.indexOf("\">");
                    int idx2 = line.lastIndexOf("</");
                    if (idx1 + 2 == idx2) {
                        errMsg += "iiop.ssl.port can not be empty.\n";
                        hasError = true;
                        continue;
                    }
                    String portStr = line.substring(idx1 + 2, idx2);
                    try {
                        int port = Integer.parseInt(portStr);
                        if (port < 1 || port > 65535) {
                             errMsg += "The value of iiop.ssl.port should be an integer from 1 to 65535\n";
                             hasError = true;
                             continue;
                        }
                    } catch (NumberFormatException e) {
                        errMsg += "The value of iiop.ssl.port must be numeric.\n";
                        hasError = true;
                        continue;
                    }
                }
                if (line.indexOf("iiop.mutualauth.port") > 0) {
                    int idx1 = line.indexOf("\">");
                    int idx2 = line.lastIndexOf("</");
                    if (idx1 + 2 == idx2) {
                        errMsg += "iiop.mutualauth.port can not be empty.\n";
                        hasError = true;
                        continue;
                    }
                    String portStr = line.substring(idx1 + 2, idx2);
                    try {
                        int port = Integer.parseInt(portStr);
                        if (port < 1 || port > 65535) {
                             errMsg += "The value of iiop.mutualauth.port should be an integer from 1 to 65535\n";
                             hasError = true;
                             continue;
                        }
                    } catch (NumberFormatException e) {
                        errMsg += "The value of iiop.mutualauth.port must be numeric.\n";
                        hasError = true;
                        continue;
                    }
                }
            }
            input.close();
        } catch (IOException ex){
            // checked already
        }
        if (hasError) {
            return errMsg;
        }
        return null;
    }

    // Life cycle control methods ///////////////////////////////////////////////////
    /**
     * Starts the installer.
     */
    public void start() {
        LogManager.logEntry("starting the installer"); // NOI18N

        Wizard.getInstance().open();

        LogManager.logExit("... finished starting the installer"); // NOI18N
    }

    /**
     * Cancels the installation. This method cancels the changes that were possibly
     * made to the components registry and exits with the cancel error code.
     *
     * This method is not logged.
     *
     * @see #finish()
     * @see #criticalExit()
     */
    public void cancel() {
        exitNormally(CANCEL_ERRORCODE);
    }

    /**
     * Finishes the installation. This method finalizes the changes made to the
     * components registry and exits with a normal error code.
     *
     * This method is not logged.
     *
     * @see #cancel()
     * @see #criticalExit()
     */
    public void finish() {
        int exitCode = NORMAL_ERRORCODE;
        final Object prop = System.getProperties().get(EXIT_CODE_PROPERTY);
        if ( prop!= null && prop instanceof Integer) {
            try {
                exitCode = ((Integer)prop).intValue();
            } catch (NumberFormatException e) {
                LogManager.log("... cannot parse exit code : " + prop, e);
            }
        }
        exitNormally(exitCode);
    }

    /**
     * Critically exists. No changes will be made to the components registry - it
     * will remain at the same state it was at the moment this method was called.
     *
     * This method is not logged.
     *
     * @see #cancel()
     * @see #finish()
     */
    public void criticalExit() {
        // exit immediately, as the system is apparently in a crashed state
        exitImmediately(CRITICAL_ERRORCODE);
    }

    public File getLocalDirectory() {
        if(localDirectory==null) {
            initializeLocalDirectory();
        }
        return localDirectory;
    }

    private void initializeErrorHandler() {
        // initialize the error manager
        LogManager.logEntry("... initializing ErrorHandler");
        ErrorManager.setFinishHandler(this);
        ErrorManager.setExceptionHandler(new ErrorManager.ExceptionHandler());

        // attach a handler for uncaught exceptions in the main thread
        Thread.currentThread().setUncaughtExceptionHandler(
                ErrorManager.getExceptionHandler());
        LogManager.logExit("... end of ErrorHandler initialization"); // NOI18N
    }

    private void initializePlatform() {
        // check whether we can safely execute on the current platform, exit in panic otherwise
        if (SystemUtils.getCurrentPlatform() == null) {
            ErrorManager.notifyCritical(ResourceUtils.getString(
                    Installer.class,
                    ERROR_UNSUPPORTED_PLATFORM_KEY));
        }
    }

    private void initializeLogManager() {
        LogManager.setLogFile(new File(getLocalDirectory(), LOG_FILE_NAME));
        LogManager.start();
    }

    private void initializeDownloadManager() {
        LogManager.logEntry("... initializing DownloadManager");
        final DownloadManager downloadManager = DownloadManager.getInstance();
        downloadManager.setLocalDirectory(getLocalDirectory());
        downloadManager.setFinishHandler(this);
        downloadManager.init();
        LogManager.logExit("... end of initializing DownloadManager");
    }

    private void initializeRegistry() {
        LogManager.logEntry("... initializing Registry");
        final Registry registry = Registry.getInstance();
        registry.setLocalDirectory(getLocalDirectory());
        registry.setFinishHandler(this);
        LogManager.logExit("... end of initializing ErrorHandler");
    }

    private void initializeWizard() {
        LogManager.logEntry("... initializing Wizard");
        final Wizard wizard = Wizard.getInstance();
        wizard.setFinishHandler(this);
        wizard.getContext().put(Registry.getInstance());
        LogManager.logExit("... end of initializing Wizard");
    }
    // private //////////////////////////////////////////////////////////////////////
    private void exitNormally(int errorCode) {
        Wizard.getInstance().close();
        DownloadManager.getInstance().terminate();
        LogManager.stop();

        exitImmediately(errorCode);
    }

    private void exitImmediately(int errorCode) {
        if (Boolean.getBoolean(DONT_USE_SYSTEM_EXIT_PROPERTY) &&
                (errorCode != CRITICAL_ERRORCODE)) {
            System.getProperties().put(
                    EXIT_CODE_PROPERTY,
                    new Integer(errorCode));
        } else {
            System.exit(errorCode);
        }
    }

    private void dumpSystemInfo() {
        // output all the possible target system information -- this may help to
        // devise the configuration differences in case of errors
        LogManager.logEntry("dumping target system information"); // NOI18N

        LogManager.logIndent("system properties:"); // NOI18N

        for (Object key: new TreeSet<Object>(System.getProperties().keySet())) {
            LogManager.log(key.toString() + " => " +  // NOI18N
                    System.getProperties().get(key).toString());
        }

        LogManager.unindent();

        LogManager.logExit("... end of target system information"); // NOI18N
    }

    private void loadEngineProperties() {
        LogManager.logEntry("loading engine properties"); // NOI18N

        try {
            LogManager.logIndent("loading engine properties");

            ResourceBundle bundle = ResourceBundle.getBundle(
                    EngineResources.ENGINE_PROPERTIES_BUNDLE);
            Enumeration <String> keys = bundle.getKeys();

            while (keys.hasMoreElements()) {
                final String key = keys.nextElement();
                final String value = bundle.getString(key);
                LogManager.log("loading " + key + " => " + value); // NOI18N
		final String currentValue = System.getProperty(key);
		if(currentValue!=null) {
                    LogManager.log("... already defined, using existing value: " + currentValue); // NOI18N
                } else {
                    System.setProperty(key,value);
                }
            }
        } catch (MissingResourceException e) {
            LogManager.log("... no engine properties file, skip loading engine properties");
        }

        LogManager.unindent();

        LogManager.logExit("... finished loading engine properties"); // NOI18N
    }

    /**
     * Parses the command line arguments passed to the installer. All unknown
     * arguments are ignored.
     *
     * @param arguments The command line arguments
     */
    private void parseArguments(String[] arguments) {
        LogManager.logEntry("parsing command-line arguments"); // NOI18N

        new CLIHandler(arguments).proceed();


        // validate arguments ///////////////////////////////////////////////////////
	/*
          Disabled since 28.02.2008 by Dmitry Lipin:
          I don`t see any reason for having that restiction at this moment:
          I can succesfully install/create bundle just with empty state file similar to
          Registry.DEFAULT_STATE_FILE_STUB_URI or even just have <state/> as the file contents
	*/
        /*
        if (UiMode.getCurrentUiMode() != UiMode.DEFAULT_MODE) {
            if (System.getProperty(
                    Registry.SOURCE_STATE_FILE_PATH_PROPERTY) == null) {
                UiMode.setCurrentUiMode(UiMode.DEFAULT_MODE);
                ErrorManager.notifyWarning(ResourceUtils.getString(
                        Installer.class,
                        WARNING_SILENT_WITHOUT_STATE_KEY,
                         SilentOption.SILENT_ARG,
                         StateOption.STATE_ARG));
            }
        }
        */
        LogManager.logExit(
                "... finished parsing command line arguments"); // NOI18N
    }

    private void initializeLocalDirectory() {
        if(localDirectory!=null) {
            return;
        }
        LogManager.logIndent("initializing the local directory"); // NOI18N

        if (System.getProperty(LOCAL_DIRECTORY_PATH_PROPERTY) != null) {
            String path = System.getProperty(LOCAL_DIRECTORY_PATH_PROPERTY);
            LogManager.log("... local directory path (initial) : " + path);
            path = SystemUtils.resolveString(path);
            LogManager.log("... local directory path (resolved): " + path);
            localDirectory = new File(path).getAbsoluteFile();
        } else {
            LogManager.log("... custom local directory was " + // NOI18N
                    "not specified, using the default"); // NOI18N

            localDirectory = 
                        new File(DEFAULT_LOCAL_DIRECTORY_PATH).getAbsoluteFile();

            System.setProperty(
                    LOCAL_DIRECTORY_PATH_PROPERTY,
                    localDirectory.getAbsolutePath());
        }

        LogManager.log("... local directory: " + localDirectory); // NOI18N

        if (!localDirectory.exists()) {
            if (!localDirectory.mkdirs()) {
                ErrorManager.notifyCritical(ResourceUtils.getString(
                        Installer.class,
                        ERROR_CANNOT_CREATE_LOCAL_DIR_KEY,
                        localDirectory));
            }
        } else if (localDirectory.isFile()) {
                ErrorManager.notifyCritical(ResourceUtils.getString(
                        Installer.class,
                        ERROR_LOCAL_DIR_IS_FILE_KEY,
                        localDirectory));
        } else if (!localDirectory.canRead()) {
                ErrorManager.notifyCritical(ResourceUtils.getString(
                        Installer.class,
                        ERROR_NO_READ_PERMISSIONS_FOR_LOCAL_DIR_KEY,
                        localDirectory));
        } else if (!localDirectory.canWrite()) {
                ErrorManager.notifyCritical(ResourceUtils.getString(
                        Installer.class,
                        ERROR_NO_WRITE_PERMISSIONS_FOR_LOCAL_DIR_KEY,
                        localDirectory));
        }

        LogManager.logUnindent(
                "... finished initializing local directory"); // NOI18N
    }

    private void createLockFile()  {
        LogManager.logIndent("creating lock file"); // NOI18N

        if (System.getProperty(IGNORE_LOCK_FILE_PROPERTY) == null) {
            final File lock = new File(getLocalDirectory(), LOCK_FILE_NAME);

            if (lock.exists()) {
                LogManager.log("... lock file already exists"); // NOI18N

                final String dialogTitle = ResourceUtils.getString(
                        Installer.class,
                        LOCK_FILE_EXISTS_DIALOG_TITLE_KEY);
                final String dialogText = ResourceUtils.getString(
                        Installer.class,
                        LOCK_FILE_EXISTS_DIALOG_TEXT_KEY);
                if(!UiUtils.showYesNoDialog(dialogTitle, dialogText)) {
                    cancel();
                }
            } else {
                try {
                    lock.createNewFile();
                } catch (IOException e) {
                    ErrorManager.notifyCritical(ResourceUtils.getString(
                            Installer.class,
                            ERROR_CANNOT_CREATE_LOCK_FILE_KEY), e);
                }

                LogManager.log("... created lock file: " + lock); // NOI18N
            }

            lock.deleteOnExit();
        } else {
            LogManager.log("... running with " + // NOI18N
                    IgnoreLockOption.IGNORE_LOCK_ARG + ", skipping this step"); // NOI18N
        }

        LogManager.logUnindent("finished creating lock file"); // NOI18N
    }

    @Deprecated
    public File cacheInstallerEngine(Progress progress) throws IOException {
        return EngineUtils.cacheEngine(progress);
    }

    private boolean isDisplayEnvSet() {
        boolean set = true;
        if (SystemUtils.isUnix()) {
            try {
                String display = System.getenv("DISPLAY");
                if (display == null) {
                    set = false;
                    System.out.println("DISPLAY is not set");
                } else {
		    System.out.println("DISPLAY is set with a value " + display);
                }
            } catch (Exception ex) {
                set = false;
            }
        } else {
	    System.out.println("IS NOT UNIX");
	}

        return set;
    }

    /////////////////////////////////////////////////////////////////////////////////
    // Constants

    // errorcodes ///////////////////////////////////////////////////////////////////
    /** Errorcode to be used at normal exit */
    public static final int NORMAL_ERRORCODE =
            0;

    /** Errorcode to be used when the installer is canceled */
    public static final int CANCEL_ERRORCODE =
            1;

    /** Errorcode to be used when the installer exits because of a critical error */
    public static final int CRITICAL_ERRORCODE =
            255;


    // lock file ////////////////////////////////////////////////////////////////////
    public static final String LOCK_FILE_NAME =
            ".nbilock"; // NOI18N

    public static final String IGNORE_LOCK_FILE_PROPERTY =
            "nbi.ignore.lock.file"; // NOI18N

    // local working directory //////////////////////////////////////////////////////
    public static final String DEFAULT_LOCAL_DIRECTORY_PATH =
            System.getProperty("user.home") + File.separator + ".nbi";

    public static final String LOCAL_DIRECTORY_PATH_PROPERTY =
            "nbi.local.directory.path"; // NOI18N

    // miscellaneous ////////////////////////////////////////////////////////////////
    public static final String DONT_USE_SYSTEM_EXIT_PROPERTY =
            "nbi.dont.use.system.exit"; // NOI18N

    public static final String EXIT_CODE_PROPERTY =
            "nbi.exit.code"; // NOI18N

    public static final String BUNDLE_PROPERTIES_FILE_PROPERTY =
            "nbi.bundle.properties.file";//NOI18N

    public static final String LOG_FILE_NAME =
            "log/" + DateUtils.getTimestamp() + ".log";

    // resource bundle keys /////////////////////////////////////////////////////////
    private static final String ERROR_UNSUPPORTED_PLATFORM_KEY =
            "I.error.unsupported.platform"; // NOI18N


    private static final String WARNING_SILENT_WITHOUT_STATE_KEY =
            "I.warning.silent.without.state"; // NOI18N

    private static final String ERROR_CANNOT_CREATE_LOCAL_DIR_KEY =
            "I.error.cannot.create.local.dir"; // NOI18N

    private static final String ERROR_LOCAL_DIR_IS_FILE_KEY =
            "I.error.local.dir.is.file"; // NOI18N

    private static final String ERROR_NO_READ_PERMISSIONS_FOR_LOCAL_DIR_KEY =
            "I.error.no.read.permissions.for.local.dir"; // NOI18N

    private static final String ERROR_NO_WRITE_PERMISSIONS_FOR_LOCAL_DIR_KEY =
            "I.error.no.write.permissions.for.local.dir"; // NOI18N

    private static final String LOCK_FILE_EXISTS_DIALOG_TITLE_KEY =
            "I.lock.file.exists.dialog.title"; // NOI18N

    private static final String LOCK_FILE_EXISTS_DIALOG_TEXT_KEY =
            "I.lock.file.exists.dialog.text"; // NOI18N

    private static final String ERROR_CANNOT_CREATE_LOCK_FILE_KEY =
            "I.error.cannot.create.lock.file"; // NOI18N

}
